# Hello NestJS

Learinig [NestJS](https://nestjs.com).

## How to run

### Requirements

- nodejs

### Installation

```bash
# Install the dependencies
yarn
```

### Development

Start docker mongodb container in another terminal

```bash
docker-compose up
```

Starts server in development

```bash
yarn start:dev
```

### Production

```bash
# clean previous build
yarn clean

# build the project
yarn build

# start the server
yarn start
```
