import { Document } from 'mongoose'
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose'

export class CreateProduct {
  constructor(
    readonly title: string,
    readonly description: string,
    readonly price: number
  ) {}
}

@Schema()
export class Product extends Document {
  @Prop({ required: true })
  title: string

  @Prop({ required: true })
  description: string

  @Prop({ required: true })
  price: number

  constructor(title: string, desc: string, price: number) {
    super()
    this.title = title
    this.description = desc
    this.price = price
  }
}

export const ProductSchema = SchemaFactory.createForClass(Product)
