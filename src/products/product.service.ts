import { Product, CreateProduct } from './product.model'
import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

@Injectable()
export class ProductService {
  products: Product[] = []

  constructor(
    @InjectModel(Product.name) private readonly productModel: Model<Product>
  ) {}

  async create(title: string, desc: string, price: number) {
    const c = new this.productModel(new CreateProduct(title, desc, price))
    const res = await c.save()
    return res.id
  }

  async getProducts() {
    const ps = await this.productModel.find().exec()
    return ps
  }

  async getSingleProduct(id: string) {
    const p = await this.findProduct(id)
    return {
      id: p.id,
      title: p.title,
      description: p.description,
      price: p.price,
    }
  }

  async updateProduct(id: string, title: string, desc: string, price: number) {
    const p = await this.findProduct(id)
    p.title = title ? title : p.title
    p.description = desc ? desc : p.description
    p.price = price ? price : p.price
    await p.save()

    return { message: `updated product ${p.id}` }
  }

  async deleteProduct(id: string) {
    return await this.productModel.deleteOne({ _id: id }).exec()
  }

  private async findProduct(id: string): Promise<Product> {
    try {
      return (await this.productModel.findOne({ _id: id }).exec()) as Product
    } catch (e) {
      throw new NotFoundException('Could not find product')
    }
  }
}
