import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
} from '@nestjs/common'
import { ProductService } from './product.service'

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  async addProduct(
    @Body('title') title: string,
    @Body('description') desc: string,
    @Body('price') price: number
  ) {
    const id = await this.productService.create(title, desc, price)
    return { id }
  }

  @Get()
  getAllProduct() {
    return this.productService.getProducts()
  }

  @Get(':id')
  getProduct(@Param('id') id: string) {
    return this.productService.getSingleProduct(id)
  }

  @Patch(':id')
  updateProduct(
    @Param('id') id: string,
    @Body('title') title: string,
    @Body('description') desc: string,
    @Body('price') price: number
  ) {
    return this.productService.updateProduct(id, title, desc, price)
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    return this.productService.deleteProduct(id)
  }
}
