import { ProductSchema, Product } from './product.model'
import { MongooseModule } from '@nestjs/mongoose'
import { Module } from '@nestjs/common'
import { ProductService } from './product.service'
import { ProductController } from './product.controller'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
  ],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}
