import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import consola from 'consola'

const PORT = 3000

async function main() {
  const app = await NestFactory.create(AppModule)
  await app.listen(PORT)
}

main().catch(consola.error)
