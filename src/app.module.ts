import { ProductModule } from './products/product.module'
import { Module } from '@nestjs/common'
import { AppService } from './app.service'
import { AppController } from './app.controller'
import { MongooseModule } from '@nestjs/mongoose'

@Module({
  imports: [
    ProductModule,
    MongooseModule.forRoot(`mongodb://127.0.0.1:27017`, {
      useNewUrlParser: true,
      auth: { user: 'miku', password: 'hatsune' },
      db: 'mikudb',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
